import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'nairobinstant',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
