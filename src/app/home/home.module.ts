import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { VipSectionComponent } from './components/vip-section/vip-section.component';
import { EscortSectionComponent } from './components/escort-section/escort-section.component';
import { AdsSectionComponent } from './components/ads-section/ads-section.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [
    HomePage, 
    HeaderComponent, 
    VipSectionComponent, 
    EscortSectionComponent, 
    AdsSectionComponent
  ],
})
export class HomePageModule {}
